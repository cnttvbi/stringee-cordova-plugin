#!/usr/bin/env node

const fs = require('fs');
const util = require('util');
const stat = util.promisify(fs.stat);

module.exports = function (ctx) {
    // Make sure android platform is part of build
    if (!ctx.opts.platforms.includes('android')) return;

    const platformRoot = path.join(ctx.opts.projectRoot, 'platforms/android');
    const apkFileLocation = path.join(platformRoot, 'build/outputs/apk/android-debug.apk');

    return stat(apkFileLocation).then(stats => {
        console.log(`Size of ${apkFileLocation} is ${stats.size} bytes`);
    });
};

module.exports = context => {
    return new Promise(resolve => {
        var IosSDKVersion = "StringeeSDK-iOS-1.3.1";
        var downloadFile = require('./downloadFile.js'),
            exec = require('./exec/exec.js');
        // Q = context.requireCordovaModule('q'), //GOC
        // deferral = new Q.defer();
        console.log('Downloading Stringee iOS SDK');

        var child_process = require('child_process'),
            exec = child_process.exec;
        downloadFile('https://static.stringee.com/sdk/' + IosSDKVersion + '.zip',
            './' + IosSDKVersion + '.zip', function (err) {
                if (!err) {
                    console.log('downloaded');
                    exec('unzip ./' + IosSDKVersion + '.zip', function (err, out, code) {
                        console.log('expanded');
                        console.log('unzip ./' + IosSDKVersion + '.zip');
                        var frameworkDir = context.opts.plugin.dir + '/src/ios/';
                        console.log('frameworkDir: ' + frameworkDir);
                        exec('mv ./' + IosSDKVersion + '/Stringee.framework ' + frameworkDir, function (err, out, code) {
                            console.log('moved Stringee.framework into ' + frameworkDir);
                            console.log('mv ./' + IosSDKVersion + '/Stringee.framework ' + frameworkDir);
                            exec('rm -r ./' + IosSDKVersion, function (err, out, code) {
                                console.log('Removed extracted dir');
                                exec('rm ./' + IosSDKVersion + '.zip', function (err, out, code) {
                                    console.log('Removed downloaded SDK');
                                    exec('rm -r ./' + '__MACOSX', function (err, out, code) {
                                        console.log('Removed __MACOSX folder');

                                        const start = Date.now();
                                        setTimeout(() => resolve(Date.now() - start), 1000);

                                    });
                                });
                            });
                        });
                    });

                }
            });


    }).then(msWaited => {
        console.log('Log Promise: ' + `${context.scriptLocation} waited ${msWaited} ms`);
    });
};
